﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reactive.Linq;

using Microsoft.Kinect;

namespace JumpDetectionUsingKinect
{
    class Program
    {
        static void Main(string[] args)
        {
            var kinect = KinectSensor.KinectSensors.Single();

            Console.WriteLine(kinect.DeviceConnectionId);

            var skeletonFrames = Observable.FromEventPattern
                <SkeletonFrameReadyEventArgs>(
                h => kinect.SkeletonFrameReady += h,
                h => kinect.SkeletonFrameReady -= h);

            var skeletons = skeletonFrames.Select(sf =>
            {
                using (var frame = sf.EventArgs.OpenSkeletonFrame())
                {
                    var sd = new Skeleton[frame.SkeletonArrayLength];
                    frame.CopySkeletonDataTo(sd);
                    return sd;
                }
            });

            var joints = from sd in skeletons
                         let tracked = sd.SingleOrDefault(
                             s => s.TrackingState == SkeletonTrackingState.Tracked)
                         where tracked != null
                         select tracked.Joints;

            var pos = from joint in joints
                      select
                          (joint[JointType.FootLeft].Position.Y
                              + joint[JointType.FootRight].Position.Y) / 2.0;

            var jumped = from p in pos.Buffer(TimeSpan.FromSeconds(1),
                                              TimeSpan.FromMilliseconds(200))
                         where p.Count() > 2
                         let lowThreshold = p.Max() - 0.5
                         select p.First() < lowThreshold && p.Last() < lowThreshold
                                    ? "jumped"
                                    : "didn't jump";

            var jumpedChanged = jumped.DistinctUntilChanged();
            var jumps = from j in jumpedChanged where j == "jumped" select j;

            jumps.Subscribe(Console.WriteLine);

            kinect.SkeletonStream.Enable();
            kinect.Start();

            Console.ReadLine();
        }
    }
}
